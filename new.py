import requests
from bs4 import BeautifulSoup

if __name__ == '__main__':
    sections = {}
    main_domain = 'http://myscore.com.ua'
    test = requests.get(main_domain)
    html = test.text
    soup = BeautifulSoup(html, "html.parser")
    menu = soup.find('div', {'id': 'menu'})
    for menu_item in menu.find_all('li'):
        link = menu_item.find('a', href=True)['href']
        sections[menu_item.text] = main_domain + link

    for key, link in sections.items():
        print('Searching', key)
        response = requests.get(link)
        soup = BeautifulSoup(response.text, "html.parser")

        print('Got response. Title is: ', soup.find('title').text)



