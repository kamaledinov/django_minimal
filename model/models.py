from django.db import models


# Create your models here.
class Game(models.Model):
    home_command = models.CharField(max_length=120)
    away_command = models.CharField(max_length=120)
    home_goals = models.IntegerField(default=0)
    away_goals = models.IntegerField(default=0)
