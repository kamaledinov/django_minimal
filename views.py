from django.shortcuts import render

users = {'anton':'giman','nick':'uulu','eugene':'ribak'}


def foo(request):

    if request.method == 'POST':
        name = request.POST.get('name', '')
        password = request.POST.get('password', '')

        if name in users.keys():
            db_password = users[name]
            if db_password == password:
                error = 'Vi avtorizovani!'
            else:
                error = 'Vvedite pravilnii parol'
        else:
            error = 'Net takogo polzovatelia'

        return render(request, 'index.html', context={
            'name': name,
            'error': error,
        })

    return render(request, 'index.html')
