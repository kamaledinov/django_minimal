**Run server:**

```bash
 django-admin runserver --pythonpath=. --settings=settings
```

**Make migrations:**

```bash
django-admin makemigrations model --pythonpath=. --settings=settings
```

**Migrate:**

```bash
django-admin migrate model --pythonpath=. --settings=settings
```

**Interactive shell:**

```bash
django-admin shell --pythonpath=. --settings=settings
```


**Base models snippets**

```python
from model.models import Game
game = Game.objects.create(a=1, b=2, c=3, d=4)

### OR
game = Game(a=1, b=2, c=3, d=4)
game.save()

### OR
game = Game()
game.a = 1
game.b = 2
game.c = 3
game.d = 4
game.save()



## Retrieve
Game.objects.get(pk=1) # -> Model
Game.objects.first()  #->  Model / 

Game.objects.all() # -> Queryset
Game.objects.filter(home_command='') # -> Queryset
# .last() #.count() #.exists() #.filter()
```
